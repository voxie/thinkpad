### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

### End of Zinit's installer chunk

#fastfetch -l $HOME/ascii/nerv.txt
fastfetch -l $HOME/ascii/bruh-anime-girl.txt --logo-color-1 blue

# PATH
export PATH="$HOME/.local/bin:$PATH"

# EXPORT
export EDITOR=nvim
export FZF_DEFAULT_OPTS='--color=fg:#ebfafa,hl:#37f499 --color=fg+:#ebfafa,bg+:#212337,hl+:#37f499 --color=info:#f7c67f,prompt:#04d1f9,pointer:#7081d0 --color=marker:#7081d0,spinner:#f7c67f,header:#323449'

# Add in zsh plugins
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions
zinit light Aloxaf/fzf-tab

# Add in snippets
zinit snippet OMZP::archlinux
zinit snippet OMZP::command-not-found
zinit snippet OMZP::sudo
zinit snippet OMZP::git
zinit snippet OMZP::extract

# load completions
autoload -Uz compinit && compinit

zinit cdreplay -q

# Keybindings
bindkey -e
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward
bindkey '^[w' kill-region

# History
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

# Aliases
alias y='paru -Syyu --noconfirm' 
alias vim='nvim'
alias v='nvim'
alias r='ranger'
alias mirror="sudo reflector -l 10 -c GB --sort rate --verbose --save /etc/pacman.d/mirrorlist && paru -Syy"

# yadm
alias ys='yadm status'
alias ya='yadm add -u :/ && yadm status'
function yc {
  yadm commit -m "updated"
  yadm status
}
alias yp='yadm push && yadm status'

# SSH
alias loki='ssh root@loki'
alias thor='ssh root@thor'
alias hela='ssh root@hela'
alias unraid='ssh root@unraid'
alias opn='ssh root@fw.local.voxie.org'

alias z='nvim ~/.zshrc'
alias ns='nslookup'
alias off='sudo shutdown -h now'
alias reboot='sudo reboot'
alias sus='sudo systemctl suspend'
alias q="nvim ~/.config/qtile/config.py"
alias sudoedit='sudo rvim'
alias smci='sudo make clean install'
alias xw='xwallpaper --zoom'
alias sz='source $HOME/.zshrc'
alias ip="ip -c -br a"
alias watch="watch -d -c "

# git
alias gc="git clone "

# dwm
alias edwm='nvim $HOME/.config/dwm/config.h'
alias vv='startx ~/.config/dwm/scripts/run.sh'

# eza is like ls but better
alias ls='eza --color=always --group-directories-first'
alias la='eza -a --color=always --group-directories-first'  
alias ll='eza -l --color=always --group-directories-first'  
alias lt='eza -aT --color=always --group-directories-first' 

alias ff='fastfetch'

function yy() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

# shell integrations
eval "$(starship init zsh)"
eval "$(fzf --zsh)"
eval "$(zoxide init --cmd cd zsh)"
#eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/base.json)"
