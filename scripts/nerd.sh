#!/bin/bash

set -e

function error {
  echo -e "\\e[91m$1\\e[39m"
  exit 1
}

error "Failed"

cat nerv.txt | sudo tee $HOME/issue > /dev/null || error "Failed"
