#!/bin/bash

set -e

function error {
  echo -e "\\e[91m$1\\e[39m"
  exit 1
}

sudo pacman -Syy
sudo pacman -S --noconfirm reflector rsync
sudo reflector -l 10 -c GB --sort rate --verbose --save /etc/pacman.d/mirror && sudo pacman -Syy

sudo pacman -S --noconfirm --needed - < package_list.txt || error "failed to install pkgs"


# aur installer
source paru.sh
paru-checkin

paru -S --noconfirm --needed - < package_list_aur.txt || error "failed to install aur list" 

# zsh
chsh -s $(which zsh) || error "chsh zsh failed"
