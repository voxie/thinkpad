#!/bin/bash

set -e

#cd /tmp
#git clone https://aur.archlinux.org/paru-bin.git
#cd paru-bin/;makepkg -si --noconfirm;cd

paru-checkin() {
  if $(pacman -Qi paru &>/dev/null); then
    echo -e "\e[92m[ 🗸 ] yay-bin is installed \e[39m"
  else
    echo -e "\e[91m[ ❌ ] yay-bin is not installed \e[39m so gonna install it now if you dont mind."
    cd /tmp || error "failed to cd /tmp"
    git clone https://aur.archlinux.org/paru-bin.git || error "failed to git aur paru-bin"
    cd paru-bin/;makepkg -si --noconfirm;cd
  fi
}

paru-checkin
