#!/bin/sh

interval=0

pkg_updates() {
  #updates=$({ timeout 20 doas xbps-install -un 2>/dev/null || true; } | wc -l) # void
  updates=$({ timeout 20 checkupdates 2>/dev/null || true; } | wc -l) # arch
  # updates=$({ timeout 20 aptitude search '~U' 2>/dev/null || true; } | wc -l)  # apt (ubuntu, debian etc)

  if [ -z "$updates" ]; then
    printf "Fully Updated"
  else
    printf "$updates"" updates"
  fi
}

wlan() {
	case "$(cat /sys/class/net/wl*/operstate 2>/dev/null)" in
	up) printf "󰤨 " ;;
	down) printf "󰤭 " ;;
	esac
}

battery(){
  printf "bat: $(cat /sys/class/power_supply/BAT0/capacity)"
}

clock(){
  printf "$(date '+%d/%m/%y - %R')"
}

while true; do
  [ $interval = 0 ] || [ $(($interval % 3600)) = 0 ] && updates=$(pkg_updates)
  interval=$((interval + 1))
  sleep 1 && xsetroot -name " $updates | $(wlan) | $(battery) | $(clock)"
done
