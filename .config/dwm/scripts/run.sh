#!/bin/sh

xbacklight -set 10 &
xwallpaper --zoom ~/pic/wall/wallhaven-x8wrel_1920x1080.png &
xset r rate 200 50 &
killall redshift
redshhift &
#setxkbmap us &
#picom -f &
clipmenud &

dash ~/.config/dwm/scripts/bar.sh &
while type dwm >/dev/null; do dwm && continue || break; done
