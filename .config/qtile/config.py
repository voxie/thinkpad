import os
import re
import socket
import subprocess
from libqtile import bar, layout, qtile, widget, hook, extension
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
#from libqtile.utils import guess_terminal

mod = "mod4"
terminal = "alacritty"
rofi = "rofi -show drun"
browser = "firefox"
file = "pcmanfm"

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "p", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    # Custom
    Key([mod], "space", lazy.spawn(rofi), desc="Launch Rofi"),
    Key([mod], "w", lazy.spawn(browser), desc="Launch Browser"),
    Key([mod], "e", lazy.spawn(file), desc="Launch File Manager"),
    Key([], "XF86MonBrightnessUp",lazy.spawn("brightnessctl s 5+")),
    Key([], "XF86MonBrightnessDown",lazy.spawn("brightnessctl s 5-")),
    Key([], "XF86AudioRaiseVolume",lazy.spawn("amixer set Master 5%+ unmute")),
    Key([], "XF86AudioLowerVolume",lazy.spawn("amixer set Master 5%- unmute")),
    Key([], "XF86AudioMute",lazy.spawn("amixer set Master togglemute")),
    Key([], "XF86AudioMicMute", lazy.spawn("amixer set Capture togglemute")),
    Key([mod, "shift"], "p", lazy.spawn("maim -s -u | xclip -selection clipboard -t image/png -i")),
    Key([], "Print", lazy.spawn("maim -s -u | xclip -selection clipboard -t image/png -i")),
    Key([mod], "v", lazy.spawn("clipmenu"), desc="clipboard management using dmenu"),

]

# Add key bindings to switch VTs in Wayland.
# We can't check qtile.core.name in default config as it is loaded before qtile is started
# We therefore defer the check until the key binding is run by using .when(func=...)
for vt in range(1, 8):
    keys.append(
        Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    )

layout_theme = {"border_width": 2,
                "margin": 5,
                "border_focus": ["#04d1f9"],
                "border_normal": ["#212337"],
                }


layouts = [
    #layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.Max(**layout_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    layout.Bsp(**layout_theme),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Tile(**layout_theme),
    layout.TreeTab(**layout_theme),
    layout.Spiral(**layout_theme, main_pane="left", clockwise=True, new_client_position="bottom", ratio=0.5),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

#groups = [Group(i) for i in "123456789"]
groups = [
    Group('1', label="", layout="spiral", matches=[Match(wm_class=["Alacritty", "kitty", "st"]),], ),
    Group('2', label="", layout="max",       matches=[Match(wm_class=["firefox", "brave", "librewolf"])]),
    Group('3', label="", layout="monadtall", matches=[Match(wm_class=["telegram-desktop"])]),
    Group('4', label="", layout="monadtall", matches=[Match(wm_class=["qBittorrent"])]),
    Group('5', label="", layout="monadtall", matches=[Match(wm_class=["discord"])]), 
    Group('6', label="", layout="monadtall"), 
    Group('7', label="", layout="monadtall"), 
    Group('8', label="", layout="monadtall"), 
    Group('9', label="", layout="spiral"), 
    Group('0', label="", layout="max")
    #Group('0', label="十", layout="max", matches=[Match(wm_class=["mpv"])])
]

for i in groups:
    keys.extend(
        [
            # mod1 + group number = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + group number = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + group number = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

widget_defaults = dict(
    font="Iosevka Nerd Font",
    fontsize=15,
    padding=5,
    background=["#1a1b26"],
    foreground=["#c0caf5"]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                #widget.CurrentLayout(),
                widget.GroupBox(
                    active="#f16c75",
                    #inactive="#f16c75",
                    #foreground="#ffffff",
                    block_highlight_text_color="#ffffff",
                    hide_unused=True,
                    highlight_method='text',
                    urgent_alert_method='text',
                    urgent_text='#f1fc79',
                    this_current_screen_border="#37f499"
                    ),
                #widget.TextBox(
                #    "󰣇 ",
                #    foreground="#04d1f9" # blue
                #    ),
                #widget.Prompt(),
                #widget.WindowName(),
                widget.Spacer(),
                #widget.Chord(
                #    chords_colors={
                #        "launch": ("#ff0000", "#ffffff"),
                #    },
                #    name_transform=lambda name: name.upper(),
                #),
                #widget.TextBox("default config", name="default"),
                #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                widget.CheckUpdates(
                    distro="Arch_checkupdates",
                    update_interval=1800,
                    foreground="#37f499", # green
                    colour_have_updates="#37f499", # green
                    display_format="  {updates}",
                    ),
                widget.TextBox(
                    "|",
                    foreground="#323449" # grey
                    ),
                widget.ThermalSensor(
                    threshold=70,
                    foreground="#ebfafa",
                    fmt=' {}'
                    ),
                widget.TextBox(
                    "|",
                    foreground="#323449" # grey
                    ),
                widget.CPU(
                    foreground="#ebfafa",
                    format='{load_percent}%', 
                    fmt='  {}'
                    ),
                widget.TextBox(
                    "|",
                    foreground="#323449" # grey
                    ),
                widget.Memory(            
                    foreground="#f7c67f",
                    format='{MemUsed:.0f}{mm}',
                    fmt='  {}',
                    measure_mem="G"
                    ),
                widget.TextBox(
                    "|",
                    foreground="#323449" # grey
                    ),
                widget.Wlan(
                    #ethernet_interface="wlp0s20f3",
                    interface="wlp0s20f3",
                    format='  {essid}',
                    foreground="#37f499", # green
                    ),
                widget.TextBox(
                    "|",
                    foreground="#323449" # grey
                    ),
                widget.Battery(              # im not done with that
                    battery=0,
                    charge_char="*",
                    format="󰁹 {char}{percent:2.0%}",
                    full_char="Fully Charged",
                    notify_below=15,
                    low_foreground="#f16c75",
                    foreground="#04d1f9" # blue
                    ),
                widget.TextBox(
                    "|",
                    foreground="#323449" # grey
                    ),
                widget.Clock(
                        format="  %d-%m-%y",
                        foreground="#a48cf2"
                        ),
                widget.TextBox(
                    "|",
                    foreground="#323449" # grey
                    ),
                widget.Clock(
                        format="  %R",
                        foreground="#f265b5"
                        ),
            ],
            25,
            #border_width=[5, 5, 5, 5],  # Draw top and bottom borders
            #border_color=["66000000", "000000", "60000000", "000000"]  # Borders are magenta
            margin =      [5, 5, 0, 5], # Draw top and bottom borders   [ top, right, bottom, left ]
        ),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.run([home])
