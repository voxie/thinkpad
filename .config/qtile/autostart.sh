#!/bin/bash

xset r rate 200 50 &
xset s off -dpms &
xwallpaper --zoom ~/pic/wall/default.png &
lxsession &
dunst &
udiskie &
xbacklight -set 100 &
setxkbmap us &
unclutter &
syncthing &
#picom -f &
#killall redshift && redshift &
#telegram-desktop &
#firefox &
