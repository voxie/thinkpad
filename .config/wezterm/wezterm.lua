--local helpers = require 'helpers'
--local config = {}
--helpers.apply_to_config(config)
--return config

local wezterm = require("wezterm")
config = wezterm.config_builder()
config = {
  automatically_reload_config = true,
  enable_tab_bar = false,
  window_close_confirmation = "NeverPrompt",
  window_decorations = "RESIZE", -- disable the title bar but enable the resizable border
  default_cursor_style = "BlinkingBar",
  --font = wezterm.font("JetBrains Mono", { weight = "Bold" }),
  font = wezterm.font("JetBrains Mono Nerd Font"),
  font_size = 10,
  window_background_opacity = 0.80,
  color_scheme = 'Eldritch',
}
return config
