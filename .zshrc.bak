export TERM="xterm-256color"
export EDITOR=nvim
export VISUEL=nvim
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:/usr/bin

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# fetches
#zfetch
#neofetch
#paleofetch
#fetchit
#pfetch
#rustmon -r 4
fastfetch

autoload -U colors && colors
#autoload -Uz promptinit && promptinit
setopt auto_cd
#prompt redhat

# history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history
mkdir -p ~/.cache/zsh && touch "$HISTFILE"

# aliases
alias nf='neofetch'
alias nfoff='neofetch --off'
alias v='nvim'
alias vim='nvim'
alias sudo='doas --'
alias y='paru -Syu --noconfirm'
alias cleanup='paru -cc'
alias sz='source ~/.zshrc'
alias reboot='doas reboot'
alias smci='doas make clean install'
alias ls='exa'
alias ll='exa -la'
alias mpv='mpv'
alias sxiv='sxiv'
alias mirror='doas reflector --verbose -l 10 -c GB --sort rate --save /etc/pacman.d/mirrorlist && sudo pacman -Syy'
alias xw='xwallpaper --zoom'
alias r='ranger'
alias watch='watch -d -c '

# shutdown, reboot and suspend
alias off='doas poweroff'
alias sus='doas systemctl suspend'
alias yoff='paru -Syu --noconfirm && doas poweroff'

# battery saver
alias baton='doas systemctl enable --now auto-cpufreq.service'
alias batoff='doas systemctl disable --now auto-cpufreq.service'

# ssh
alias pve='ssh root@10.0.0.2'
alias pihole='ssh dietpi@192.168.0.106'
alias deepcool='ssh jay@10.0.0.10'

# yadm
alias ys='yadm status'
alias ya='yadm add -u :/ && yadm status'
function yc {
  echo "Please enter your commit:"
  read commit
  yadm commit -m "$commit"
  yadm status
}
alias yp='yadm push && yadm status'

# qtile
alias q="nvim ~/.config/qtile/config.py"

# git
alias gc='git clone '

# completions
autoload -Uz compinit && compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
_comp_options+=(globdots)

# like vim bindkey
KEYTIMEOUT=1
bindkey -v
bindkey '^E' end-of-line
bindkey '^A' beginning-of-line
bindkey '^R' history-incremental-pattern-search-backward

# source
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-fzf-plugin/fzf.plugin.zsh 2>/dev/null

# starship
eval "$(starship init zsh)" 2>/dev/null

